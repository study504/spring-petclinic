================================================================================
===        Spring PetClinic sample application - MySQL Configuration         ===
================================================================================

@author Sam Brannen
@author Costin Leau
<<<<<<< HEAD
@author Dave Syer
=======
<<<<<<< HEAD
@author Dave Syer
=======
>>>>>>> ramo1
>>>>>>> bf78014b2763d6d8ed2e1af92351a80c167119ac

--------------------------------------------------------------------------------

1) Download and install the MySQL database (e.g., MySQL Community Server 5.1.x),
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> bf78014b2763d6d8ed2e1af92351a80c167119ac
   which can be found here: https://dev.mysql.com/downloads/. Or run the
   "docker-compose.yml" from the root of the project (if you have docker installed
   locally):

        $ docker-compose up
        ...
        mysql_1_eedb4818d817 | MySQL init process done. Ready for start up.
        ...

2) (Once only) create the PetClinic database and user by executing the "db/mysql/user.sql"
   scripts. You can connect to the database running in the docker container using 
   `mysql -u root -h localhost --protocol tcp`, but you don't need to run the script there
   because the petclinic user is already set up if you use the provided `docker-compose.yml`.

3) Run the app with `spring.profiles.active=mysql` (e.g. as a System property via the command
   line, but any way that sets that property in a Spring Boot app should work). For example use
   
   mvn spring-boot:run -Dspring-boot.run.profiles=mysql

   To activate the profile on the command line.

N.B. the "petclinic" database has to exist for the app to work with the JDBC URL value
as it is configured by default. This condition is taken care of automatically by the 
docker-compose configuration provided, or by the `user.sql` script if you run that as
root.
<<<<<<< HEAD
=======
=======
   which can be found here: http://dev.mysql.com/downloads/

2) Download Connector/J, the MySQL JDBC driver (e.g., Connector/J 5.1.x), which
   can be found here: http://dev.mysql.com/downloads/connector/j/
   Copy the Connector/J JAR file (e.g., mysql-connector-java-5.1.5-bin.jar) into
   the db/mysql directory. Alternatively, uncomment the mysql-connector from the
   Petclinic pom.

3) Create the PetClinic database and user by executing the "db/mysql/createDB.txt"
   script.

4) Open "src/main/resources/jdbc.properties"; comment out all properties in the
   "HSQL Settings" section; uncomment all properties in the "MySQL Settings"
   section.
>>>>>>> ramo1
>>>>>>> bf78014b2763d6d8ed2e1af92351a80c167119ac
